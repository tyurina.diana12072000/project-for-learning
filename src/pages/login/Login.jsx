import "./login.css";
import { Link } from "react-router-dom";
import React, {FC, useState} from 'react';


const Login  = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

const sendTestData = (email,password) =>{
  console.log(email,password)
}
  return (
    <div className="login">
      <span className="loginTitle">Авторизация</span>
      <form className="loginForm">
        <label>Email</label>
        <input 
        onChange={e => setEmail (e.target.value)}
        value={email}
        className="loginInput" type="text" placeholder="Введите email" />
        <label>Пароль</label>
        <input 
        onChange={e => setPassword (e.target.value)}
        value={password}
        className="loginInput" type="password" placeholder="Введите пароль" />
        <button onClick={() => sendTestData(email,password)} className="loginButton">Войти</button>
      </form>
        
    </div>
  );
};
export default Login;