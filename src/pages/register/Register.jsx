import "./register.css"
import { Link } from "react-router-dom";

export default function Register() {
    return (
        <div className="register">
      <span className="registerTitle">Регистрация</span>
      <form className="registerForm">
        <label>Имя</label>
        <input className="registerInput" type="text" placeholder="Введите имя" />
        <label>Email</label>
        <input className="registerInput" type="text" placeholder="Введите email" />
        <label>Пароль</label>
        <input className="registerInput" type="password" placeholder="Введите пароль" />
        <button className="registerButton">Зарегистрироваться</button>
      </form>
        
    </div>
    )
}