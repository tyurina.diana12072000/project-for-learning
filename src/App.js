// import React, {FC, useContext, useEffect, useState} from 'react';
import {TopBar} from "./components/topbar/TopBar.jsx";
import Register from "./pages/register/Register";
import * as React from "react";
import { Routes, Route } from "react-router-dom";
import Setting from "./pages/settings/Setting";
import Home from "./pages/home/Home";
import Login from "./pages/login/Login";
import Write from "./pages/write/Write";
import Single from "./pages/single/Single";
import {Footer} from "./components/footer/Footer.jsx"


export function App() {
  const user = false;
  return (
 
 <div>
    <TopBar/> 
    <Routes>
      <Route index element={<Home/>}/>
      <Route path='/register' element={user ? <Home/> : <Register/>}/>
      <Route path='/login' element={user ? <Home/> : <Login/>}/>
      <Route path='/write' element={user ? <Write/> :<Register/>}/>
      <Route path='/settings' element={user ? <Setting/> :<Register/>}/>
      <Route path='/post/:postId' element={<Single/>}/>
    </Routes>
    <Footer/>
    </div>
    
  );
}


