import "./sidebar.css";

export default function SideBar() {
  return (
    <div className="sidebar">
      <div className="sidebarItem">
        <span className="sidebarTitle">КАЛЬКУЛЯТОР</span>
        <img
          src="https://yandex-images.clstorage.net/BD4m92H00/2702a2PX3/TJaIq_u6C0UoxDs3jyqOHS3OaZ1JlUM8DmFq2_X9KWzzIFmvktFuRp8wat6UwN5Vi4qfUGVtF0lpm0l6pNiRjhmvybrL1nJ5AMA7O7sO4jkx4kmhaYUTIAcANra4rWQI91FVj4dtg4ofocsnOGBW5SKqj3BlYDcscdbk1pEtQFt8BhA7d-ZCbaDMnidPPg4hZTf5TQ0EKwmuFUOWZH0NSGfhjE4bWcMaRwnnn1RIcgGWarNwVThOWKGm1GvVzZxTRCYIb8uaYklcdO67j77Swen_dSFhrMPJXlXXDiw1wTiv-YgSb41r1lsJPzdIjKrxEnqX5MWcngzxwgT_0bWsD-QmFKOnUobZoDxyP4-WekGRc9kptRSvWfrlk36I0RXIw-FU9iN5y07f-X8HoAyadUrWExgtrJL8ITasN7VpeIOc6jDLMz72WYTk4ot3HrqtKQ99aeUgvynGhXOi3HlhMNMZvMrrRV_Kh7GHF9yA2vky2kNwrUCSUCUygI_NFcD_4MK0898-mmkIwLJr96JmVSkjZUFBfD81Sm3H8pCFAWhrbdRK8y1vpo8Vk0c4zH51ElrT-OksTjCJ3lgnqa0A99wqpOv3_uqt3ABSp_u6coX5O0Xl6XDjKdLJn3Io0e2ML1k4Lucpn9rLFXu3UAzanQ4eX3A91M44BbK8E0WF7JMMVpRrP-KKsQDYRhfvFvJlFc-RvW0EwxViWdPO6NXFTG89JHLbCVNms8HzQ7TY7k2yhqtMPax2PFnqyM8FLRRfEJqg36smpslQMDaDexZSlWV3QSGVJEeN5uUzbphdFahfZUhWHwX3IoNJ-1d0oMaBSl5jfPlI0rT5qiifBVkA09TWXH9TPtbx-NjGz4OC6rGld_2xiSx3BebV67L8EcHs223AbtuZ75LPfWMrwAhe7UIuI3SpOE7w5Qrcm401lHsAmuhzm9rSHchYbqfn6rLtjeNlZc3s1wHqaWOiKE04"
          alt=""
        />
        <p>
          Loremdnfkjdhikfjrahjf;arjfoi;iudrigj;H ipsum dolor sit hic officiis
          oditraesentium, doloribus labore rem perferendis ipsum neque aliquid
          corrupti omnis!
        </p>
      </div>
      <div className="sidebarItem">
        <span className="sidebarTitle">ПОКРЫТИЯ</span>
        <ul className="sidebarList">
          <li className="sidebarListItem">Напольная плитка</li>
          <li className="sidebarListItem">Настенная плитка</li>
          <li className="sidebarListItem">Линолеум</li>
          <li className="sidebarListItem">Обои</li>
          <li className="sidebarListItem">Ламинат</li>
          {/* <li className="sidebarListItem">Cinema</li> */}
        </ul>
      </div>
      <div className="sidebarItem">
        <span className="sidebarTitle">FOLLOW US</span>
        <div className="sidebarSocial">
          <i className="sidebarIcon fab fa-facebook-square"></i>
          <i className="sidebarIcon fab fa-twitter-square"></i>
          <i className="sidebarIcon fab fa-pinterest-square"></i>
          <i className="sidebarIcon fab fa-instagram-square"></i>
        </div>
      </div>
    </div>
  );
}
