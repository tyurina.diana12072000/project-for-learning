import "./post.css"

export default function Post() {
  return (
  <div className="post">
      <img 
      className="postImg"
      src="https://data2.1freewallpapers.com/download/interior-graphic-living-room-2800x2100.jpg" 
      alt=""
      />
      <div className="postInfo">
        <div className="postCats">
          <span className="postCat">Дизайн</span>
          <span className="postCat">Ремонт</span>
        </div>
        <span className="postTitle">
        Белый цвет и его сочетания в интерьере
          </span>
          <hr/>
          <span className="postData">1 час назад</span>
      </div>
      <p className="postDesc">Белый цвет – ахроматический, это идеальный нейтральный фон, ведь он не относится ни к холодной, ни к теплой палитре. Он визуально увеличивает помещение и уместен в большинстве стилей интерьера. По Фэн-шуй белый считается символом бесконечности и чистоты.</p>
          </div>
  )
}


