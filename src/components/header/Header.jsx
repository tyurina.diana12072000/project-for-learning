import "./header.css";
import krIMG from '../img/kraski.png'

export default function Header() {
  return (
    < div className="header">
      < div className="headerTitles">
        < span className="headerTitleSm">Ваш личный помощник в ремонте</span>
        < span className="headerTitleLg">СтройHelp</span>
      </div>
      <img
        className="headerImg"
        src={krIMG}
        alt=""
      />
    </div>
  );
}
