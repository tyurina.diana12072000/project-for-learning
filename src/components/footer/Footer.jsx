import "./footer.css";

import React from 'react'

export  function Footer() {
  return (
    <div className="footer">
      <div className="footerLeft">

      <form className="messageForm">
        <label className="fontBold">Кратко о нас</label>
        <label>В разработанном веб-приложении представлен калькулятор, который наиболее точно рассчитает 
          оптимальное количество материалов, минимизирует остатки, а также сэкономит время на расчет. Также вы можете разместить свою статью и ознакомиться с уже написанными на тему ремонта.</label>

        <label  className="fontBold">Контакты</label>
        <li className="listContakt"> <i class="fa-solid fa-mobile-screen-button"></i> 8 800 555 35 35</li>
        <li className="listContakt"> <i class="fa-regular fa-at"></i>qwerty@mail.ru </li>
      </form>
      </div>

      <div className="footerCenter">
      <form className="messageForm">
        <label>Ваше имя</label>
        <input className="messageInput" type="text" placeholder="Введите ваше имя" />
        <label>Email</label>
        <input className="messageInput" type="email  " placeholder="email@mail.ru" />
      </form>
      </div>
      <div className="footerRight">
      <form className="messageForm">
        <label>Сообщение</label>
        <input className="messageInputD" type="text" placeholder="Ваше сообщение..." />
        <button className="messageButton">Отправить</button>
      </form>
      </div>
      
    </div>
  )
}
