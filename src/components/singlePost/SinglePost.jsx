
import "./singlePost.css";

export default function SinglePost() {
  return (
    <div className="singlePost">
        <div className="singlePostWrapper">
        <img
          className="singlePostImg"
          src="https://images.pexels.com/photos/6685428/pexels-photo-6685428.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
          alt=""
        />
        <h1 className="singlePostTitle">
          Lorem ipsum dolor
          <div className="singlePostEdit">
            <i className="singlePostIcon far fa-edit"></i>
            <i className="singlePostIcon far fa-trash-alt"></i>
          </div>
        </h1>
        <div className="singlePostInfo">
          <span className="singlePostAuthor">
            Author: <b > Diana </b>
          </span>
          <span className="singlePostDate">1 hour ago</span>
        </div>
        <p className="singlePostDesc">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Possimus culpa eveniet nihil. Fuga maxime aliquam eaque neque officia exercitationem beatae aperiam, enim rerum id, velit dolores! Officia, numquam tenetur! Molestias. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi minus eligendi nulla eum sit non blanditiis magni mollitia cupiditate reprehenderit modi incidunt, inventore aliquid, ad perferendis molestias in eveniet culpa. Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut, in blanditiis laudantium labore fugit exercitationem voluptatum, natus beatae neque ab repudiandae doloremque distinctio quas. Cum nulla reiciendis assumenda beatae tempora?</p>
        </div>
     </div>
  )
}
      